#/bin/sh
# List the most active gadget editors on Wikimedia sites
# This code is licensed under CC0 1.0 Universal: https://creativecommons.org/publicdomain/zero/1.0/legalcode

DEADLINE=20161120000000 # how far to go back; must be YYYYMMDDHHMMSS

#Temporary files (too lazy to pass wget to jq via stdin):
TMPFILE_RAWSITEMATRIX=tmp_wmf_rawsitematrix.json
TMPFILE_DOMAINSLIST=tmp_wmf_domainmatrix.list

# As output is appended into the files, remove those files if existing, to avoid duplicated entries
# in case the script got ever run and interrupted in the past and left those files in place:
if [ -f "$TMPFILE_RAWSITEMATRIX" ]; then
  rm $TMPFILE_RAWSITEMATRIX
fi
if [ -f "$TMPFILE_DOMAINSLIST" ]; then
  rm $TMPFILE_DOMAINSLIST
fi

##### BEGIN: Create file TMPFILE_DOMAINSLIST that lists the domains of all our sites

# Get domain info for all sites from https://en.wikipedia.org/w/api.php?action=sitematrix&smsiteprop=url and dump into TMPFILE_RAWSITEMATRIX
# Docs: https://en.wikipedia.org/w/api.php?action=help&modules=sitematrix
wget -q https:\/\/en.wikipedia.org\/w\/api.php?action=sitematrix\&smsiteprop=url\&format=json -O $TMPFILE_RAWSITEMATRIX

# Dump all key names (such as "47" or "specials") in our sitematrix into tmpkeyarray
mapfile -t tmpkeyarray < <(jq '.sitematrix | keys' $TMPFILE_RAWSITEMATRIX | jq ".[]")
for i in "${tmpkeyarray[@]}"
do
  # Check only items like "123" (which are actually languages). Handle the "specials" key later as it has a different format 
  if [[ $i =~ ^\"[0-9]+\"$ ]]; then
    # Some languages might have zero domains so the sites[] array is empty; do not add them
    if [[ $(cat $TMPFILE_RAWSITEMATRIX | jq -r '.sitematrix .'"$i"' .site[] .url') ]]; then
      # Output all domains and drop them into another file. 
      echo "$(cat $TMPFILE_RAWSITEMATRIX | jq -r '.sitematrix .'"$i"' .site[] .url')" >> $TMPFILE_DOMAINSLIST
    fi
  fi
done

# Now also add the non-language sites listed under the "specials" key:
specialslength="$(cat $TMPFILE_RAWSITEMATRIX | jq '.sitematrix .specials | length')"
for (( i=1; i<$specialslength; i++ ))
do
  echo "$(cat $TMPFILE_RAWSITEMATRIX | jq -r '.sitematrix .specials['$i'] .url')" >> $TMPFILE_DOMAINSLIST
done

rm $TMPFILE_RAWSITEMATRIX

##### END: Create file TMPFILE_DOMAINSLIST which is a complete list of all our domains

# Dump all the domains from the file into an array:
mapfile -t sitesArray < "$TMPFILE_DOMAINSLIST"

for WEBSITEURL in "${sitesArray[@]}"; do

  # Strip https:// prefix for saving local files
  WEBSITENAME=${WEBSITEURL:8}

  # Get a list of all MediaWiki:Gadget-* pages on site: 
  curl -s "$WEBSITEURL/w/api.php?action=query&list=allpages&apfrom=Gadget&apto=Gadgeu&aplimit=500&apnamespace=8&apprefix=Gadget-&format=json" > $WEBSITENAME-tmp1a
  cat $WEBSITENAME-tmp1a | jq -r '.. |."title"? | select(. != null)' > $WEBSITENAME-tmpallgadgetstranslations

  # TODO: Horrible, needs proper recursion but I'm too stupid to save variable outside of the loop because it's bash (API only offers 500 results due to pagination):
  CMCONTINUE1=`cat $WEBSITENAME-tmp1a | jq -r .continue.cmcontinue`
  if [[ "$CMCONTINUE1" != 'null' ]]; then
    curl -s "$WEBSITEURL/w/api.php?action=query&list=allpages&apfrom=Gadget&apto=Gadgeu&aplimit=500&apnamespace=8&apprefix=Gadget-&format=json&cmcontinue=$CMCONTINUE1" > $WEBSITENAME-tmp1b
    CMCONTINUE2=`cat $WEBSITENAME-tmp1b | jq -r .continue.cmcontinue`
    cat $WEBSITENAME-tmp1b | jq -r '.. |."title"? | select(. != null)' >> $WEBSITENAME-tmpallgadgetstranslations
    if [[ "$CMCONTINUE2" != 'null' ]]; then
      curl -s "$WEBSITEURL/w/api.php?action=query&list=allpages&apfrom=Gadget&apto=Gadgeu&aplimit=500&apnamespace=8&apprefix=Gadget-&format=json&cmcontinue=$CMCONTINUE2" > $WEBSITENAME-tmp1c
      cat $WEBSITENAME-tmp1c | jq -r '.. |."title"? | select(. != null)' >> $WEBSITENAME-tmpallgadgetstranslations
    fi
  fi

  sleep 2

  # Only get the actual JS pages, no translations etc:
  grep -o '.*\.js$' $WEBSITENAME-tmpallgadgetstranslations > $WEBSITENAME-allgadgets

  # Cannot query revisions for several pages (titles) in one request. So for each page, list the authors of its revisions since $DEADLINE (see API:Revisions).
  # Must install jq and iterate over each line in $WEBSITENAME-allgadgets to get the author of each edit on that gadget code since $DEADLINE. Drop "-r" if you want each name encapsulated in apostrophes.
  while read gadget; do
    echo "Processing $WEBSITEURL/wiki/$gadget"
    curl -s "$WEBSITEURL/w/api.php?action=query&prop=revisions&titles=$gadget&rvprop=user&formatversion=2&rvlimit=max&rvend=$DEADLINE&format=json" | jq -r '.. |."user"? | select(. != null)' >> $WEBSITENAME-editornames
    sleep 1
  done <$WEBSITENAME-allgadgets
  # And finally also check MediaWiki:Common.js:
  echo "Processing $WEBSITEURL/wiki/MediaWiki:Common.js"
  curl -s "$WEBSITEURL/w/api.php?action=query&prop=revisions&titles=MediaWiki:Common.js&rvprop=user&formatversion=2&rvlimit=max&rvend=$DEADLINE&format=json" | jq -r '.. |."user"? | select(. != null)' >> $WEBSITENAME-editornames
  sleep 1

  if [ -e $WEBSITENAME-editornames ]; then
    cat $WEBSITENAME-editornames >> output.txt
  fi

  # clean up:
  rm -f $WEBSITENAME-tmp1* $WEBSITENAME-tmpallgadgetstranslations
  rm -f $WEBSITENAME-allgadgets $WEBSITENAME-editornames

  sleep 1
done

# clean up:
rm $TMPFILE_DOMAINSLIST

# Sort names by most edits
sort output.txt | uniq -c | sort -bnr > output-sorted.txt

# If only the names are wanted, cut the first eight characters from each line. Note that "eight" might change, hence check first.
cat output-sorted.txt |  sed 's/^........//' > output-names-only.txt
